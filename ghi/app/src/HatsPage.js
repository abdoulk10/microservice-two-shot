import React, { useState, useEffect } from 'react';
import HatDetails from './HatDetails';

function HatList() {
  const [hats, setHats] = useState([]);
  const [selectedHat, setSelectedHat] = useState(null);

  useEffect(() => {
    fetchHats();
  }, []);

  const fetchHats = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleHatClick = hat => {
    setSelectedHat(hat);
  };

  const handleDelete = async hatId => {
    try {
      const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`, {
        method: 'DELETE',
      });

      if (response.ok) {
        await fetchHats();
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const renderHatsList = () => {
    return (
      <div className="row">
        {hats.map(hat => (
          <div className="col-md-4 mb-4" key={hat.id}>
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">{hat.style_name}</h5>
                <p className="card-text">{hat.color}</p>
                <button
                  className="btn btn-danger ms-2"
                  onClick={() => handleDelete(hat.id)}
                >
                  Delete
                </button>
                <button
                  className="btn btn-primary ms-2"
                  onClick={() => handleHatClick(hat)}
                >
                  Details
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  };

  return (
    <div className="container">
      <h1 className="display-4 text-center my-4">Hats List</h1>
      {selectedHat ? (
        <div>
          <button
            className="btn btn-primary mb-2"
            onClick={() => setSelectedHat(null)}
          >
            Back to List
          </button>
          <HatDetails hat={selectedHat} />
        </div>
      ) : (
        renderHatsList()
      )}
    </div>
  );
}

export default HatList;
