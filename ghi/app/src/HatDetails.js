import React from 'react';

function HatDetails({ hat }) {
  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">{hat.style_name}</h5>
        <p className="card-text">Fabric: {hat.fabric}</p>
        <p className="card-text">Style: {hat.style_name}</p>
        <p className="card-text">Color: {hat.color}</p>
        <p>Picture:</p>
        <img src={hat.picture_url} alt={hat.style_name} className="img-fluid" />
        <p>URL: {hat.picture_url}</p>
      </div>
    </div>
  );
}

export default HatDetails;
