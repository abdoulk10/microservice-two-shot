import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsPage from './HatsPage';
import CreateHat from './CreateHat';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsPage />} />
          <Route path="/create_hats" element={<CreateHat />} />
          <Route path="/shoes" element={<ShoeList shoes={props.shoes} />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
  }



export default App;
