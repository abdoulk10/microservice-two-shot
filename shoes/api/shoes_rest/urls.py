from django.urls import path

from .api_views import shoes_list_view, shoe_detail_view

urlpatterns = [
    path('shoes/', shoes_list_view, name='api_list_shoes'),
    path('shoes/<int:pk>/', shoe_detail_view, name='api_detail_shoes')
]
